"use server";

import "server-only";
import { db } from "~/server/db";
import { auth } from "@clerk/nextjs/server";
import { images } from "~/server/db/schema";
import { and, eq } from "drizzle-orm";
import { redirect } from "next/navigation";
import { revalidatePath, revalidateTag } from "next/cache";
import analyticsServerClient from "~/server/analytics";

export async function getMyImages() {
  const user = auth();

  if (!user.userId) throw new Error("Not authenticated");

  return await db.query.images.findMany({
    where: (model, { eq }) => eq(model.userId, user.userId),
    orderBy: (model, { desc }) => desc(model.createdAt),
  });
}

export async function getImage(id: number) {
  const user = auth();

  if (!user.userId) throw new Error("Not authenticated");

  const image = await db.query.images.findFirst({
    where: (model, { eq, and }) =>
      and(eq(model.userId, user.userId), eq(model.id, id)),
  });

  if (!image) {
    redirect("/");
  }

  return image;
}

export async function deleteImage(id: number) {
  const user = auth();
  if (!user.userId) throw new Error("Not authenticated");

  try {
    await db
      .delete(images)
      .where(and(eq(images.id, id), eq(images.userId, user.userId)));

    analyticsServerClient.capture({
      distinctId: user.userId,
      event: "delete image",
      properties: {
        imageId: id,
      },
    });
  } catch (e) {
    throw new Error("Failed to delete image");
  }

  redirect("/");
}
