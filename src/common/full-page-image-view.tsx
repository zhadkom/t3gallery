import { deleteImage, getImage } from "~/server/queries";
import { clerkClient } from "@clerk/nextjs/server";
import { Button } from "~/components/ui/button";

export async function FullPageImageView(props: { photoId: string }) {
  const idAsNumber = Number(props.photoId);
  if (Number.isNaN(idAsNumber)) throw new Error("Invalid photo id");

  const img = await getImage(idAsNumber);

  const uploaderInfo = await clerkClient.users.getUser(img.userId);
  return (
    <div className="flex h-full w-full min-w-0 flex-1 justify-center">
      <div className="flex w-full items-center justify-center border-r">
        <img
          src={img.url}
          alt={img.name}
          className="max-h-screen flex-shrink object-contain"
        />
      </div>
      <div className="flex w-72 flex-shrink-0 flex-col items-start p-4">
        <h1 className="p-2 text-center text-lg">{img.name}</h1>
        <div className="flex flex-col p-2">
          <span>Uploaded by:</span>
          <span>{uploaderInfo.fullName}</span>
        </div>

        <div className="flex flex-col p-2">
          <span>Created on:</span>
          <span>{new Date(img.createdAt).toLocaleDateString()}</span>
        </div>
        <div className="p-2">
          <form
            action={async () => {
              "use server";
              await deleteImage(idAsNumber);
            }}
          >
            <Button variant="destructive">Delete</Button>
          </form>
        </div>
      </div>
    </div>
  );
}
