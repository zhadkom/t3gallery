import { getMyImages } from "~/server/queries";
import Image from "next/image";
import Link from "next/link";

export const dynamic = "force-dynamic";
export default async function HomePage() {
  const images = await getMyImages();

  return (
    <main className="">
      <div className="flex flex-wrap justify-center gap-4 p-4">
        {images.map((image) => (
          <div key={image.id} className="flex w-48 flex-col gap-2">
            <Link href={`/img/${image.id}`}>
              <Image
                width={192}
                height={192}
                style={{ objectFit: "cover" }}
                src={image.url}
                alt={image.name}
              />
            </Link>
            <p>{image.name}</p>
          </div>
        ))}
      </div>
    </main>
  );
}
