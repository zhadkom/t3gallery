"use client";

import { SignedIn, SignedOut, SignInButton, UserButton } from "@clerk/nextjs";
import Link from "next/link";
import { CustomUploadButton } from "~/app/_components/upload-button";

export function TopNav() {
  return (
    <nav className="flex items-center justify-between border-b border-white bg-gray-800 p-4 text-xl font-semibold text-white">
      <Link href={"/"}>
        <h1 className="text-2xl font-bold">T3 Gallery</h1>
      </Link>
      <div className="flex flex-row items-center gap-4">
        <SignedOut>
          <SignInButton />
        </SignedOut>
        <SignedIn>
          <CustomUploadButton />
          <UserButton />
        </SignedIn>
      </div>
    </nav>
  );
}
