# T3 Gallery

## TODO

- [x] Make it deploy (vercel)
- [x] Scaffold ui with mock data
- [x] Tidy up build process
- [x] Set up a database (vercel postgres)
- [x] Attach database to ui
- [x] Add authentication (w/ clerk)
- [x] Add image upload
- [x] "taint" (server-only)
- [x] Use Next Image component
- [x] Error handling (w/ Sentry)
- [x] Routing/image page (parallel route)
- [x] Update upload button
- [x] ShadUI (toast)
- [x] Delete feat (w/ Server Actions)
- [x] Analytics (posthog)
- [x] Rate limiting (upstash)